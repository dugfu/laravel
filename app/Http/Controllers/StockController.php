<?php

namespace App\Http\Controllers;

use App\Stock;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Stocks\stock', ['stocks' => Stock::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Stocks\create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stock = new Stock();
        $stock->nom = $request->input('nom');
        if ($request->input('longueur') > $request->input('largeur')){
            $stock->largeur = $request->input('largeur');
            $stock->longueur = $request->input('longueur');
        }else{
            $stock->longueur = $request->input('largeur');
            $stock->largeur = $request->input('longueur');
        }
        if( $request->input('place')){
            $stock->place = true;
        }else{
            $stock->place = false;
        }

        $stock->save();
        return redirect()->route('stocks', ['boolinsert' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function show(Stock $stock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('Stocks\edit', ['stock' => Stock::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $stock = Stock::findOrFail($id);
        $stock->nom = $request->input('nom');
        if ($request->input('longueur') > $request->input('largeur')){
            $stock->largeur = $request->input('largeur');
            $stock->longueur = $request->input('longueur');
        }else{
            $stock->longueur = $request->input('largeur');
            $stock->largeur = $request->input('longueur');
        }
        if( $request->input('place')){
            $stock->place = true;
        }else{
            $stock->place = false;
        }
        $stock->save();
        return redirect()->route('stocks', ['boolupdate' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Createur  $createur
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        return view('Stocks\suppr', ['stock' => Stock::findOrFail($id)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stock = Stock::findOrFail($id);
        $stock->delete();
        return redirect()->route('stocks', ['booldelete' => true]);
    }
}
