<?php

namespace App\Http\Controllers;

use App\Objet;
use Illuminate\Http\Request;

class ObjetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Objets\objet', ['objets' => Objet::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Objet  $objet
     * @return \Illuminate\Http\Response
     */
    public function show(Objet $objet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Objet  $objet
     * @return \Illuminate\Http\Response
     */
    public function edit(Objet $objet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Objet  $objet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Objet $objet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Objet  $objet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Objet $objet)
    {
        //
    }
}
