<?php

use Illuminate\Database\Seeder;

class CreateurSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Createur::class, 10)->create();
    }
}
