<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(CreateurSeeder::class);
        $this->call(SalleSeeder::class);
         $this->call(StockSeeder::class);
    }
}
