<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Createur;
use Faker\Generator as Faker;

$factory->define(Createur::class, function (Faker $faker) {
    return [
        'nom' => $faker->lastName,
        'prenom' => $faker->firstName,
        'pseudo' => $faker->word
    ];
});
