<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Stock;

$factory->define(Stock::class, function (Faker $faker) {
    return [
        'nom' => $faker->word,
        'largeur' => $faker->numberBetween(1,100),
        'longueur' => $faker->numberBetween(1,100),
        'place' => $faker->boolean
    ];
});
