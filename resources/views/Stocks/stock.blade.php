@extends('layout')

@section('title','Stocks')

@section('content')

    <table class="table table-dark table-striped p-2">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nom</th>
            <th>Longueur</th>
            <th>Largeur</th>
            <th>Place</th>
            <th>SVG</th>
            <th>Modifier</th>
            <th>Supprimer</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($stocks as $stock)
            <tr>
                <td>{{ $stock->id }}</td>
                <td>{{ $stock->nom }}</td>
                <td>{{ $stock->longueur }}</td>
                <td>{{ $stock->largeur }}</td>
                <td>{{ $stock->place }}</td>
                <td>{{ $stock->SVG }}</td>
                <td>
                    <a href="{{ route('stocks.edit', ['id' => $stock->id]) }}" class="btn btn-primary" style="cursor: pointer;">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
                </td>
                <td>
                    <a href="{{ route('stocks.delete', ['id' => $stock->id]) }}" class="btn btn-danger" style="cursor: pointer;">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @if (app('request')->input('boolinsert'))
        L'objet a été ajouté au stock.
    @endif
    @if (app('request')->input('boolupdate'))
        L'objet a été modifié.
    @endif
    @if (app('request')->input('booldelete'))
        L'objet a été supprimé.
    @endif
@endsection

