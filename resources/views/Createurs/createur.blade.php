@extends('layout')

@section('title','Créateurs')

@section('content')

    <table class="table table-dark table-striped p-2">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nom</th>
            <th>Prenom</th>
            <th>Pseudo</th>
            <th>Modifier</th>
            <th>Supprimer</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($createurs as $createur)
            <tr>
                <td>{{ $createur->id }}</td>
                <td>{{ $createur->nom }}</td>
                <td>{{ $createur->prenom }}</td>
                <td>{{ $createur->pseudo }}</td>
                <td>
                    <a href="{{ route('createurs.edit', ['id' => $createur->id]) }}" class="btn btn-primary" style="cursor: pointer;">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
                </td>
                <td>
                    <a href="{{ route('createurs.delete', ['id' => $createur->id]) }}" class="btn btn-danger" style="cursor: pointer;">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @if (app('request')->input('boolinsert'))
        Le créateur a été ajouté.
    @endif
    @if (app('request')->input('boolupdate'))
        Le créateur a été modifié.
    @endif
    @if (app('request')->input('booldelete'))
        Le créateur a été supprimé.
    @endif
@endsection

