@extends('layout')

@section('title','Salles')

@section('content')
    <form method="post" action="{{ route('salles.destroy', ['id' => $salle->id]) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <label for="idCreateur">Select list (select one):</label>
        <select class="form-control" id="idCreateur" name="idCreateur" disabled>
            @foreach($createurs as $createur)
            <option
                    value="{{ $createur->id }}"
                    @if ($salle->idCreateur == $createur->id)
                        selected
                    @endif
            >
                {{ $createur->prenom }} {{ $createur->nom }} [{{ $createur->pseudo }}]
            </option>
            @endforeach
        </select>
        <div class="form-group">
            <label for="nom">Nom :</label>
            <input type="text" name="nom" class="form-control" placeholder="Entrer nom" id="nom" value="{{ $salle->nom }}" disabled>
        </div>
        <div class="form-group">
            <label for="longueur">Longueur (en mètres) :</label>
            <input type="number" name="longueur" class="form-control" placeholder="Entrer longueur (m)" pattern="[0-9]+([\.,][0-9]+)?" step="0.01" id="longueur" value="{{ $salle->longueur }}" disabled>
        </div>
        <div class="form-group">
            <label for="largeur">Largeur (en mètres) :</label>
            <input type="number" name="largeur" class="form-control" placeholder="Entrer largeur (m)"  pattern="[0-9]+([\.,][0-9]+)?" step="0.01" id="largeur" value="{{ $salle->largeur }}" disabled>
        </div>
        <div class="container-fluid">
            <p>
                Êtes-vous sur de vouloir supprimer cette salle ?<br/>
                <button type="submit" class="btn btn-danger">Supprimer</button>
                <a class="btn btn-primary" href="{{ route('salles') }}">Annuler</a>
            </p>
        </div>
    </form>
@endsection
